'use strict';

let postList = document.querySelector('.post-list');

class Card{
  constructor(name, email, title, body, id){
    this.name = name
    this.email = email
    this.title = title
    this.body = body
    this.id = id
  }

  createPost(){

    let post = document.createElement('div');
    post.classList.add('post');
    postList.append(post);

    let userInfo = document.createElement('div');
    userInfo.classList.add('user-info');
    post.append(userInfo);

    let userName = document.createElement('span');
    userName.classList.add('user-name');
    userName.innerText = this.name;
    userInfo.append(userName);

    let emailWrapper = document.createElement('span');
    let userEmail = document.createElement('a');
    userEmail.classList.add('user-email');
    userEmail.href = "#";
    userEmail.innerText = this.email;
    userInfo.append(emailWrapper);
    emailWrapper.append(userEmail);

    let titlePost = document.createElement('h2');
    titlePost.classList.add('title')
    titlePost.innerText = this.title;
    userInfo.after(titlePost);

    let postText = document.createElement('p');
    postText.innerText = this.body;
    titlePost.after(postText);

    let btn = document.createElement('button');
    btn.classList.add('card-btn');
    btn.innerText = 'hide post'
    emailWrapper.after(btn);

    btn.addEventListener("click", event => {
      event.preventDefault();
      fetch(`https://ajax.test-danit.com/api/json/posts/${this.id}`, {
        method: "DELETE",
      }).then(() => post.remove());
    });
  }
}


async function getUsers(){
  let response = await fetch ('https://ajax.test-danit.com/api/json/users');
  let usersList = await response.json();
  return usersList;
}

async function getPosts(){
  let users = await getUsers();
  let response = await fetch('https://ajax.test-danit.com/api/json/posts');
  let posts = await response.json();

  for(let n of users){
    for(let i of posts){
      if(i.userId === n.id){
        let post = new Card(n.name, n.email, i.title, i.body, i.id)
        post.createPost();
      }
    }
  }
}

let loader = document.querySelector('.loader');

setTimeout(async function () {
  await getPosts();
  console.log('Done!');
  loader.classList.add("hidden");
}, 500);











